/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author ASUS
 */
public class WaterDispenserPlus {
    private boolean isOn, lidOpen, waterRunning;
    private int waterLevel, maxLevel = 100;
    private Scanner reader = new Scanner(System.in);
    
    public WaterDispenserPlus(int waterLevel) {
        if (waterLevel <= maxLevel && waterLevel >= 0) {
            this.waterLevel = waterLevel;
        } else if (waterLevel > maxLevel) {
            this.waterLevel = 100;
        } else {
            System.out.println("Negative value not accepted.");
            this.waterLevel = 0;
        }
        System.out.println("A dispenser is made with " + this.waterLevel + " units of water inside.");
    }
    
    public void pressWaterButton() {
        if (!isOn || waterLevel < 1) {
            return;
        } else {
            waterRunning = true;
            System.out.println("Please press any button to stop the dispenser.");
            Thread pourWater = pour();
            String cmd = reader.nextLine();
            if (!cmd.isEmpty()) {
                waterRunning = false;
                System.out.println("The water dispenser has " + this.waterLevel + " units of water inside.");
            }
        }
    }
    
    public Thread pour() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (waterRunning) {
                    System.out.println("Pouring water...");
                    try {
                        while (waterLevel > 0 && waterRunning) {
                            System.out.println("*");
                            Thread.sleep(500);
                            waterLevel--;
                        }
                    } catch (Exception e) {
                        System.out.println("Interrupted Process.");
                    }
                }
            }
        });
        thread.start();
        return thread;
    }
    
    public void pressOnOff() {
        isOn = !isOn;
        if (isOn) {
            System.out.println("The dispenser is turning on.");
        } else {
            System.out.println("The dispenser is turning off.");
        }
    }
    
    public void fillTank(int amount) {
        if (lidOpen) {
            if (waterLevel + amount > maxLevel) {
                waterLevel = maxLevel;
                System.out.println("The dispenser is full now.");
            } else {
                waterLevel += amount;
                System.out.println("The dispenser has " + waterLevel + " units of water now.");
            }
        } else {
            System.out.println("Lid is closed. Do you want to open and fill tank? (y/n)");
            String cmd = reader.nextLine();
            if (!cmd.toLowerCase().startsWith("y")) {
                return;
            }
            System.out.println("Opening the lid to fill.");
            lidOpen = !lidOpen;
            fillTank(amount);
        }
    }
    
    public void fillTank() {
        fillTank(maxLevel);
    }
}
