/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;

public class CDPlayer {
    private boolean powerOn, trayOpen, isPlaying, trackPlaying;
    private int volume, track;
    private Disc disc;
    Scanner reader = new Scanner(System.in);
    
    public CDPlayer() {
        this.disc = null;
        this.volume = 5;
    }
    
    public void turnOnOff() {
        powerOn = !powerOn;
        if (powerOn) {
            System.out.println("CD Player turning on.");
            return;
        }
        stop();
        System.out.println("CD Player turning off.");
    }
    
    public void upVolume() {
        if (!powerOn) {
            return;
        }
        if (volume < 20) {
            volume++;
        }
        System.out.println("Current volume: " + this.volume);
    }

    public void downVolume() {
        if (!powerOn) {
            return;
        }
        if (volume > 0) {
            volume--;
        }
        System.out.println("Current volume: " + this.volume);
    } 
    
    public void openCloseTray(Disc disc) {
        if (powerOn) {
            stop();
            if (!trayOpen) {
                trayOpen = true;
                isPlaying = false;
                System.out.println("The disc tray is opening.");
                if (isEmpty()) {
                    System.out.println("Do you want to put a disc in?(y/n)");
                    String cmd = reader.nextLine();
                    if (cmd.toLowerCase().startsWith("y")) {
                        insertDisc(disc);
                    }
                } else {
                    System.out.println("Do you want take the disc out?(y/n)");
                    String cmd = reader.nextLine();
                    if (cmd.toLowerCase().startsWith("y")) {
                        removeDisc(disc);
                    }
                }
            }
            trayOpen = false;
            System.out.println("The disc tray is closing.");
            return;
        }
        System.out.println("The CD Player is off.");
    }
    
    public void insertDisc(Disc disc) {
        if (powerOn && trayOpen) {
            this.disc = disc;
            System.out.println(disc.getDiscName() + " has been inserted.");
        } else if (!powerOn) {
            System.out.println("The CD Player is off.");
        } else {
            System.out.println("The tray is closed.");
        }
    } 
    
    public void removeDisc(Disc disc) {
        if (powerOn && trayOpen) {
            System.out.println(disc.getDiscName() + " has been removed.");
            this.disc = null;
        } else if (!powerOn) {
            System.out.println("The CD Player is off.");
        } else {
            System.out.println("The tray is closed.");
        }
    }
    
    public void play() {
        if (!powerOn) {
            System.out.println("The CD Player is off.");
        } else if (trayOpen) {
            System.out.println("The disc tray is open. Please close it.");
        } else if (isEmpty()) {
            System.out.println("There is no disc inside the CD Player.");
        } else {
            trackPlaying = true;
            Thread play = startTrack();
            System.out.println("Press 6 to stop.");
        }
    }
    
    public boolean isEmpty() {
        return this.disc == null;
    }
    
    public void next() {
        track++;
    }
    
    public void back() {
        
    }
    
    public void stop() {
        trackPlaying = false;
    }
    
    public Thread startTrack() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (trackPlaying) {
                    track = 0;
                    try {
                        while (track < disc.getTrackList().size()) {
                            if (!trackPlaying) {
                                break;
                            }
                            System.out.println(disc.getSong(track) + " is playing.");
                            System.out.println("...");
                            Thread.sleep(2000);
                            if (track == 3) {
                                track = -1;
                            }
                            track++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    
                }
            }
        });
        thread.start();
        return thread;
    }
}

//System.out.println(s + " is playing.");
//System.out.println("...");
//Thread.sleep(2000);