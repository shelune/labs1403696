/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class CoffeeMaker {
    private boolean isOn, isClean;
    private int waterLevel, beanAmount, timeUsed;
    private final int maxWaterLevel = 100, maxBeanAmount = 20;
    
    public CoffeeMaker(int waterLevel, int beanAmount) {
        if (waterLevel < 0) {
            waterLevel = 0;
        }
        if (waterLevel > maxWaterLevel) {
            waterLevel = maxWaterLevel;
        }
        if (beanAmount < 0) {
            beanAmount = 0;
        }
        if (beanAmount > maxBeanAmount) {
            beanAmount = maxBeanAmount;
        }
        this.isClean = true;
        this.waterLevel = waterLevel;
        this.beanAmount = beanAmount;
    }
    
    public void turnOnOff() {
        if (isOn) {
            System.out.println("The Coffee Maker is turning off.");
        } else {
            System.out.println("The Coffee Maker is turning on.");
        } 
        isOn = !isOn;
        rinse();
    }
    
    public void fillWaterTank(int amount) {
        if (amount < 0) {
            System.out.println("Negative value not accepted.");
            return;
        }
        if (this.waterLevel + amount > maxWaterLevel) {
            fillWaterTank();
        } else {
            waterLevel += amount;
        }
        System.out.println("The Coffee Maker now has " + waterLevel + " units of water.");
    }
    
    public void fillWaterTank() {
        waterLevel = maxWaterLevel;
        System.out.println("The Coffee Maker now has " + waterLevel + " units of water.");
    }
    
    public void fillBeanTank(int amount) {
        if (amount < 0) {
            System.out.println("Negative value not accepted.");
            return;
        }
        if (this.beanAmount + amount > maxBeanAmount) {
            fillBeanTank();
        } else {
            beanAmount += amount;
        }
        System.out.println("The Coffee Maker now has " + beanAmount + " beans.");
    }
    
    public void fillBeanTank() {
        beanAmount = maxBeanAmount;
    }
    
    public void pressWater() {
        if (isOn) {
            if (this.waterLevel >= 5) {
                this.waterLevel -= 5;
                timeUsed++;
                System.out.println("A cup of water is served.");
                System.out.println(this);
                isClean = false;
                autoRinse();
            } else {
                System.out.println("Water level is too low. Please fill the tank.");
            }
        } else {
            System.out.println("The Coffee Maker is not on. Please turn it on.");
        }
    }
    
    public void pressRegularCoffee() {
        if (isOn) {
            if (this.beanAmount >= 3 && this.waterLevel >= 5) {
                this.waterLevel -= 5;
                this.beanAmount -= 3;
                timeUsed++;
                System.out.println("A cup of regular coffee is served.");
                System.out.println(this);
                isClean = false;
                autoRinse();
            } else {
                if (this.waterLevel < 5) {
                    System.out.println("Water level is too low. Please fill the water tank.");
                }
                if (this.beanAmount < 3) {
                    System.out.println("Too few beans left. Please fill the bean tank.");
                }
            }
        } else {
            System.out.println("The Coffee Maker is not on. Please turn it on.");
        }
    }
    
    public void pressEspresso() {
        if (isOn) {
            if (this.beanAmount >= 4 && this.waterLevel >= 10) {
            this.waterLevel -= 10;
                this.beanAmount -= 4;
                timeUsed++;
                System.out.println("A cup of espresso is served");
                System.out.println(this);
                isClean = false;
                autoRinse();
            } else {
                if (this.waterLevel < 10) {
                    System.out.println("Water level is too low. Please fill the water tank.");
                }
                if (this.beanAmount < 4) {
                    System.out.println("Too few beans left. Please fill the bean tank.");
                }
            }
        } else {
            System.out.println("The Coffee Maker is not on. Please turn it on.");
        }
    }
    
    public void rinse() {
        if (timeUsed == 0) {
            return;
        }
        if (waterLevel < 3) {
            System.out.println("Water level is too low for rinsing. Please fill the tank.");
        } else {
            isClean = true;
            waterLevel -= 2;
            System.out.println("The Coffee Maker is now rinsed.");
        }
    }
    
    public void autoRinse() {
        int rinseCheck = this.timeUsed % 5;
        if (rinseCheck == 0) {
            rinse();
            System.out.print("Auto-rinsing.");
        } else {
            System.out.println("The Coffee Maker will rinse itself in " + (5 - rinseCheck) + " more uses.");
        }
    }
    
    public void getStatus() {
        if (isClean) {
            System.out.println("The Coffee Maker is clean.");
        } else {
            System.out.println("The Coffee Maker is not clean.");
        }
    }
    
    public String toString() {
        return "The Coffee Maker now has " + this.waterLevel + " units of water and " + this.beanAmount + " beans.";
    }
    
    
}
