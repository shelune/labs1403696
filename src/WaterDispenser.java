/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class WaterDispenser {
    private boolean isOn, lidOpen;
    int waterLevel, maxLevel = 100;
    Thread th = new Thread();
    
    public WaterDispenser(int waterLevel) {
        if (waterLevel <= maxLevel && waterLevel >= 0) {
            this.waterLevel = waterLevel;
        } else if (waterLevel > maxLevel) {
            this.waterLevel = 100;
        } else {
            System.out.println("Negative value not accepted.");
            this.waterLevel = 0;
        }
        System.out.println("A dispenser is made with " + this.waterLevel + " units of water inside.");
    }
    
    public void pressWaterButton(int amount) {
        if (amount < 0) {
            System.out.println("You are not Jesus!");
        } else {
            if (isOn && waterLevel > 0) {
                if (waterLevel < amount) {
                    System.out.println(waterLevel + " units of water has been poured out. The dispenser is empty.");
                    waterLevel = 0;
                } else {
                    waterLevel -= amount;
                    System.out.println(amount + " has been poured out. The dispenser has " + waterLevel + " units of water left.");
                }
            } else {
                if (!isOn) {
                    System.out.println("The dispenser if off. Please turn on.");
                }
                if (waterLevel == 0) {
                    System.out.println("The dispenser is empty. Please fill.");
                }
            }  
        }
    }
    
    public void pressOnOff() {
        isOn = !isOn;
    }
    
    public void fillTank(int amount) {
        if (lidOpen) {
            if (waterLevel + amount > maxLevel) {
                waterLevel = maxLevel;
                System.out.println("The dispenser is full now.");
            } else {
                waterLevel += amount;
                System.out.println("The dispenser has " + waterLevel + " units of water now.");
            }
        } else {
            lidOpen = !lidOpen;
            System.out.println("Lid is closed. Opening the lid to fill.");
            fillTank(amount);
        }
    }
    
    public void fillTank() {
        fillTank(maxLevel);
    }
}
