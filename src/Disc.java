/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author ASUS
 */
public class Disc {
    private ArrayList<String> trackList = new ArrayList<String>();
    private String discName;
    
    public Disc(String name, int number) {
        for (int i = 0; i < number; i++) {
            trackList.add("Song " + (i+1));
        }
        this.discName = name;
    }
    
    public int trackListLength() {
        return trackList.size();
    }
    
    public ArrayList<String> getTrackList() {
        return this.trackList;
    }
    
    public String getDiscName() {
        return this.discName;
    }
    
    public String getSong(int index) {
        return trackList.get(index);
    }
    
    public String toString() {
        String result = "";
        for (String s : trackList) {
            result += s + ", ";
        }
        return this.discName + " has: " + result.substring(0, result.length() - 2) + ".";
    }
}
