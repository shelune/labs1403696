// This is not an exercise but a "sandbox" where you can freely test
// whatever you want
import java.util.InputMismatchException;
import java.util.Scanner;

public class User {
    
    public static Scanner reader = new Scanner(System.in);
    public static void main(String[] args) {

        // Water Dispenser 01
        
        /*
        WaterDispenser wd = new WaterDispenser(5);
        WaterDispenser wd1 = new WaterDispenser(102);
        WaterDispenser wd2 = new WaterDispenser(-10);
        
        System.out.println("===");
        
        wd.pressWaterButton(6);
        wd.pressOnOff();
        wd.pressWaterButton(4);
        wd.pressWaterButton(15);
        wd.fillTank(100);
        
        System.out.println("===");
        
        wd2.pressWaterButton(15);
        wd2.pressOnOff();
        wd2.pressWaterButton(10);
        wd2.fillTank(10);
        
        System.out.println("===");
        
        wd1.pressWaterButton(15);
        wd1.pressOnOff();
        wd1.pressWaterButton(2);
        wd1.fillTank();
        wd1.pressWaterButton(99);
                
        */
        // Coffee Maker 02
        /*
        CoffeeMaker cm = new CoffeeMaker(50, 15);
        CoffeeMaker cm1 = new CoffeeMaker(120, 40);
        CoffeeMaker cm2 = new CoffeeMaker(-120, -5);
        
        System.out.println(cm);
        
        cm.turnOnOff();
        cm.pressWater();
        cm.fillBeanTank(-20);
        cm.fillWaterTank(-150);
        
        System.out.println("");
        
        cm1.turnOnOff();
        cm1.pressEspresso();
        
        System.out.println("");
        
        cm2.turnOnOff();
        cm2.pressRegularCoffee();
        cm2.fillBeanTank(40);
        cm2.fillWaterTank(15); */
        
        // CDPlayer 03
        
        /*
        int cmd = 0;
        
        Disc thisDisc = generateDisc("Disc 1", 4);
        CDPlayer player = new CDPlayer();
        System.out.println("Disc available _ " + thisDisc);
        
        while (true) {
            try {
                cdPlayerInstructions();
                cmd = reader.nextInt();
                testInput(cmd, 7);
            } catch (InputMismatchException e) {
                System.out.println("Please input number as indicated.");
                reader.nextLine();
            } catch (InvalidNumberException e) {
                System.out.println(e);
            }

            switch (cmd) {
                case -1:
                    return;
                case 1:
                    player.turnOnOff();
                    break;
                case 2:
                    player.openCloseTray(thisDisc);
                    break;
                case 3:
                    player.play();
                    break;
                case 4:
                    player.upVolume();
                    break;
                case 5:
                    player.downVolume();
                    break;
                case 6:
                    player.stop();
                    break;
                case 7:
                    player.next();
                    break;
            }
        } */
        
        // Water Dispenser Plus
        WaterDispenserPlus wdp = new WaterDispenserPlus(20);
        int cmd = 0;
        while (true) {
            try {
                waterDispenserInstructions();
                cmd = reader.nextInt();
                testInput(cmd, 6);
            } catch (InputMismatchException e) {
                System.out.println("Please input number as indicated");
                reader.nextLine();
            } catch (InvalidNumberException e) {
                System.out.println(e);
            }
            
            switch (cmd) {
                case -1:
                    return;
                case 1:
                    wdp.pressOnOff();
                    break;
                case 2:
                    wdp.pressWaterButton();
                    break;
                case 3:
                    wdp.fillTank();
                    break;
            }
        }
    }
    
    public static Disc generateDisc(String name, int numberOfSongs) {
        Disc disc = new Disc(name, numberOfSongs);
        return disc;
    }
    
    public static void cdPlayerInstructions() {
        System.out.println("For purposes, only Disc 1 is available, please add more disks if you want.");
        System.out.println("Choose a button (1-7) | Press -1 to exit CD Player.");
        System.out.println("1. Turn On / Off");
        System.out.println("2. Open / Close Tray");
        System.out.println("3. Play");
        System.out.println("4. Increase Volume");
        System.out.println("5. Decrease Volume");
        System.out.println("6. Stop");
        System.out.println("7. Next");
    }
    
    public static void waterDispenserInstructions() {
        System.out.println("Press numbers available below to server yourself water.");
        System.out.println("Until you press something and Enter, water will continue flowing out.");
        System.out.println("1. Turn On / Off");
        System.out.println("2. Water");
        System.out.println("3. Fill Water");
    }
    
    public static void testInput(int number, int limit) throws InvalidNumberException {
        int upperLim = limit;
        if (number > upperLim || number < 1 && (number != -1)) {
            throw new InvalidNumberException();
        }
    } 

}

class InvalidNumberException extends Exception {
    
    public InvalidNumberException() {
        super();
    }
    
    public String getMessage() {
        return "Only accept number within the range indicated.";
    }
}

